//
//  NativeAdsViewController.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 6/19/17.
//  Copyright © 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit
import GoogleMobileAds

class NativeAdsViewController: UIViewController, GADNativeExpressAdViewDelegate {

    @IBOutlet weak var adView: GADNativeExpressAdView!
//    let nrOfAds = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.adView.adUnitID = "ca-app-pub-4520006002223164/7546238533"
        self.adView.rootViewController = self
        self.adView.delegate = self
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        self.adView.load(request)
        
    }
}
